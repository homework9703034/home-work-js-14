function toggleContent(id) {
  const tabs = document.getElementsByClassName("tabs-title");
  for (let i = 1; i <= 5; i++) {
    const content = document.getElementById(i);
    const element = document.getElementById(`a${i}`);
    element.classList.remove("active");
    if (i === id) {
      content.style.display = "block";
    } else {
      content.style.display = "none";
    }
  }

  const activeElement = document.getElementById(`a${id}`);
  activeElement.classList.add("active");
}
const btn = document.querySelector(".btn");
const tabs = document.querySelectorAll(".tabs-title");

let isStyleChanged =
  JSON.parse(localStorage.getItem("isStyleChanged")) || false;

const storedStyle = JSON.parse(localStorage.getItem("style")) || [];

function applyStyles() {
  if (isStyleChanged) {
    for (let i = 0; i < tabs.length; i++) {
      tabs[i].style.color = storedStyle[i];
    }
    document.body.style.background = "blue";
  }
}

applyStyles();

function changeStyle() {
  if (isStyleChanged) {
    for (let i = 0; i < tabs.length; i++) {
      tabs[i].style.color = storedStyle[i];
    }
    document.body.style.background = "white";
    isStyleChanged = false;
  } else {
    storedStyle.length = 0;
    for (let i = 0; i < tabs.length; i++) {
      storedStyle.push(tabs[i].style.color);
      tabs[i].style.color = "red";
    }
    localStorage.setItem("style", JSON.stringify(storedStyle));
    document.body.style.background = "blue";
    isStyleChanged = true;
  }

  localStorage.setItem("isStyleChanged", isStyleChanged);
}

btn.addEventListener("click", changeStyle);
